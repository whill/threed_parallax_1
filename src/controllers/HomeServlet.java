package controllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 * @author win hill
 * @webservlet
 *   name=HomeServlet
 */
@WebServlet(urlPatterns = {"/Home"})
public class HomeServlet extends HttpServlet implements Serializable {

	private static final long serialVersionUID = -1L;

	@Override
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
		HashMap<String, String> homeHash = new HashMap<String, String>();
		
		homeHash.put( "pageName", "home" );
		homeHash.put( "userName", "Win" );
		
		request.setAttribute( "pageAttributes", homeHash );
		request.getRequestDispatcher( "/home.jsp" ).forward( request, response );
	}
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		HashMap<String, String> homeHash = new HashMap<String, String>();
		
		homeHash.put( "pageName", "home" );
		homeHash.put( "userName", "Win" );
		
		request.setAttribute( "pageAttributes", homeHash );
		request.getRequestDispatcher( "/home.jsp" ).forward( request, response );
	}
}