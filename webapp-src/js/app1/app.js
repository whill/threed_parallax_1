/**
 * 
 */
define( function( require ) {
	var
		THREE = require( 'three' ),
		camera = require( 'camera' ),
		controls = require( 'controls' ),
		geometry = require( 'geometry' ),
		light = require( 'light' ),
		material = require( 'material' ),
		renderer = require( 'renderer' ),
		scene = require( 'scene' ),
		app = {
			//...
		};
	
	return app;
} );