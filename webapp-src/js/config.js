/**
 * Configure Require.js
 */
var require = {
	// Default load path for js files
	baseUrl : 'js/',
	shim : {
		// --- Use shim to mix together all THREE.js subcomponents {js/threeSubComponents/three.js}
		'threeCore' : { exports : 'THREE' },
		'TrackballControls' : { deps: ['threeCore'], exports : 'THREE' },
    	'OrbitControls' : { deps : ['three'], exports : 'THREE' },
		// --- end THREE sub-components
		'detector' : { exports : 'Detector' },
		'stats' : { exports : 'Stats' }
	},
	// Third party code lives in js/lib
	paths : {
		// --- start THREE sub-components
		three : 'threeSubComponents/three',
		threeCore : 'libs/three.min',
		TrackballControls : 'libs/controls/TrackballControls',
		OrbitControls : 'libs/OrbitControls',
		// --- end THREE sub-components
		detector : 'utils/three/Detector',
		stats : 'libs/stats.min',
		// Require.js plugins
		text : 'libs/text',
		shader : 'libs/shader',
		// Where to look for shader files
		shaders : 'shaders',
		jquery : [
		            '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min',
		            /* If the CDN location fails, load from this location */
		            'libs/jquery-2.1.0.min'
				],
		app1 : 'app1/app'
	}
};