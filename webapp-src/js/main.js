/**
 * Starting an app is controlled within main
 */
require( ['detector', 'app1', 'container'], function( $ ) {
		
	if ( ! Detector.webgl ) {
			Detector.addGetWebGLMessage();
	};
} );